// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Employee.proto

package ProtoFiles.Employee;

/**
 * Protobuf type {@code ProtoFiles.Employee.Employee}
 */
public final class Employee extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:ProtoFiles.Employee.Employee)
    EmployeeOrBuilder {
private static final long serialVersionUID = 0L;
  // Use Employee.newBuilder() to construct.
  private Employee(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private Employee() {
    name_ = "";
    buildingCode_ = "";
    floorNumber_ = 0;
    department_ = "";
    cafeteriaCode_ = "";
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new Employee();
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private Employee(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    if (extensionRegistry == null) {
      throw new java.lang.NullPointerException();
    }
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          case 8: {

            employeeId_ = input.readUInt32();
            break;
          }
          case 18: {
            java.lang.String s = input.readStringRequireUtf8();

            name_ = s;
            break;
          }
          case 26: {
            java.lang.String s = input.readStringRequireUtf8();

            buildingCode_ = s;
            break;
          }
          case 32: {
            int rawValue = input.readEnum();

            floorNumber_ = rawValue;
            break;
          }
          case 40: {

            salary_ = input.readUInt32();
            break;
          }
          case 50: {
            java.lang.String s = input.readStringRequireUtf8();

            department_ = s;
            break;
          }
          case 58: {
            java.lang.String s = input.readStringRequireUtf8();

            cafeteriaCode_ = s;
            break;
          }
          default: {
            if (!parseUnknownField(
                input, unknownFields, extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return ProtoFiles.Employee.EmployeeOuterClass.internal_static_ProtoFiles_Employee_Employee_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return ProtoFiles.Employee.EmployeeOuterClass.internal_static_ProtoFiles_Employee_Employee_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            ProtoFiles.Employee.Employee.class, ProtoFiles.Employee.Employee.Builder.class);
  }

  public static final int EMPLOYEE_ID_FIELD_NUMBER = 1;
  private int employeeId_;
  /**
   * <code>uint32 Employee_Id = 1;</code>
   * @return The employeeId.
   */
  @java.lang.Override
  public int getEmployeeId() {
    return employeeId_;
  }

  public static final int NAME_FIELD_NUMBER = 2;
  private volatile java.lang.Object name_;
  /**
   * <code>string Name = 2;</code>
   * @return The name.
   */
  @java.lang.Override
  public java.lang.String getName() {
    java.lang.Object ref = name_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      name_ = s;
      return s;
    }
  }
  /**
   * <code>string Name = 2;</code>
   * @return The bytes for name.
   */
  @java.lang.Override
  public com.google.protobuf.ByteString
      getNameBytes() {
    java.lang.Object ref = name_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      name_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int BUILDING_CODE_FIELD_NUMBER = 3;
  private volatile java.lang.Object buildingCode_;
  /**
   * <code>string Building_Code = 3;</code>
   * @return The buildingCode.
   */
  @java.lang.Override
  public java.lang.String getBuildingCode() {
    java.lang.Object ref = buildingCode_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      buildingCode_ = s;
      return s;
    }
  }
  /**
   * <code>string Building_Code = 3;</code>
   * @return The bytes for buildingCode.
   */
  @java.lang.Override
  public com.google.protobuf.ByteString
      getBuildingCodeBytes() {
    java.lang.Object ref = buildingCode_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      buildingCode_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int FLOOR_NUMBER_FIELD_NUMBER = 4;
  private int floorNumber_;
  /**
   * <code>.ProtoFiles.Employee.Floor Floor_Number = 4;</code>
   * @return The enum numeric value on the wire for floorNumber.
   */
  @java.lang.Override public int getFloorNumberValue() {
    return floorNumber_;
  }
  /**
   * <code>.ProtoFiles.Employee.Floor Floor_Number = 4;</code>
   * @return The floorNumber.
   */
  @java.lang.Override public ProtoFiles.Employee.Floor getFloorNumber() {
    @SuppressWarnings("deprecation")
    ProtoFiles.Employee.Floor result = ProtoFiles.Employee.Floor.valueOf(floorNumber_);
    return result == null ? ProtoFiles.Employee.Floor.UNRECOGNIZED : result;
  }

  public static final int SALARY_FIELD_NUMBER = 5;
  private int salary_;
  /**
   * <code>uint32 Salary = 5;</code>
   * @return The salary.
   */
  @java.lang.Override
  public int getSalary() {
    return salary_;
  }

  public static final int DEPARTMENT_FIELD_NUMBER = 6;
  private volatile java.lang.Object department_;
  /**
   * <code>string Department = 6;</code>
   * @return The department.
   */
  @java.lang.Override
  public java.lang.String getDepartment() {
    java.lang.Object ref = department_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      department_ = s;
      return s;
    }
  }
  /**
   * <code>string Department = 6;</code>
   * @return The bytes for department.
   */
  @java.lang.Override
  public com.google.protobuf.ByteString
      getDepartmentBytes() {
    java.lang.Object ref = department_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      department_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int CAFETERIA_CODE_FIELD_NUMBER = 7;
  private volatile java.lang.Object cafeteriaCode_;
  /**
   * <code>string cafeteria_code = 7;</code>
   * @return The cafeteriaCode.
   */
  @java.lang.Override
  public java.lang.String getCafeteriaCode() {
    java.lang.Object ref = cafeteriaCode_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      cafeteriaCode_ = s;
      return s;
    }
  }
  /**
   * <code>string cafeteria_code = 7;</code>
   * @return The bytes for cafeteriaCode.
   */
  @java.lang.Override
  public com.google.protobuf.ByteString
      getCafeteriaCodeBytes() {
    java.lang.Object ref = cafeteriaCode_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      cafeteriaCode_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (employeeId_ != 0) {
      output.writeUInt32(1, employeeId_);
    }
    if (!com.google.protobuf.GeneratedMessageV3.isStringEmpty(name_)) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 2, name_);
    }
    if (!com.google.protobuf.GeneratedMessageV3.isStringEmpty(buildingCode_)) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 3, buildingCode_);
    }
    if (floorNumber_ != ProtoFiles.Employee.Floor.UNKNOWN.getNumber()) {
      output.writeEnum(4, floorNumber_);
    }
    if (salary_ != 0) {
      output.writeUInt32(5, salary_);
    }
    if (!com.google.protobuf.GeneratedMessageV3.isStringEmpty(department_)) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 6, department_);
    }
    if (!com.google.protobuf.GeneratedMessageV3.isStringEmpty(cafeteriaCode_)) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 7, cafeteriaCode_);
    }
    unknownFields.writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (employeeId_ != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeUInt32Size(1, employeeId_);
    }
    if (!com.google.protobuf.GeneratedMessageV3.isStringEmpty(name_)) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(2, name_);
    }
    if (!com.google.protobuf.GeneratedMessageV3.isStringEmpty(buildingCode_)) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(3, buildingCode_);
    }
    if (floorNumber_ != ProtoFiles.Employee.Floor.UNKNOWN.getNumber()) {
      size += com.google.protobuf.CodedOutputStream
        .computeEnumSize(4, floorNumber_);
    }
    if (salary_ != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeUInt32Size(5, salary_);
    }
    if (!com.google.protobuf.GeneratedMessageV3.isStringEmpty(department_)) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(6, department_);
    }
    if (!com.google.protobuf.GeneratedMessageV3.isStringEmpty(cafeteriaCode_)) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(7, cafeteriaCode_);
    }
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof ProtoFiles.Employee.Employee)) {
      return super.equals(obj);
    }
    ProtoFiles.Employee.Employee other = (ProtoFiles.Employee.Employee) obj;

    if (getEmployeeId()
        != other.getEmployeeId()) return false;
    if (!getName()
        .equals(other.getName())) return false;
    if (!getBuildingCode()
        .equals(other.getBuildingCode())) return false;
    if (floorNumber_ != other.floorNumber_) return false;
    if (getSalary()
        != other.getSalary()) return false;
    if (!getDepartment()
        .equals(other.getDepartment())) return false;
    if (!getCafeteriaCode()
        .equals(other.getCafeteriaCode())) return false;
    if (!unknownFields.equals(other.unknownFields)) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (37 * hash) + EMPLOYEE_ID_FIELD_NUMBER;
    hash = (53 * hash) + getEmployeeId();
    hash = (37 * hash) + NAME_FIELD_NUMBER;
    hash = (53 * hash) + getName().hashCode();
    hash = (37 * hash) + BUILDING_CODE_FIELD_NUMBER;
    hash = (53 * hash) + getBuildingCode().hashCode();
    hash = (37 * hash) + FLOOR_NUMBER_FIELD_NUMBER;
    hash = (53 * hash) + floorNumber_;
    hash = (37 * hash) + SALARY_FIELD_NUMBER;
    hash = (53 * hash) + getSalary();
    hash = (37 * hash) + DEPARTMENT_FIELD_NUMBER;
    hash = (53 * hash) + getDepartment().hashCode();
    hash = (37 * hash) + CAFETERIA_CODE_FIELD_NUMBER;
    hash = (53 * hash) + getCafeteriaCode().hashCode();
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static ProtoFiles.Employee.Employee parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static ProtoFiles.Employee.Employee parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static ProtoFiles.Employee.Employee parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static ProtoFiles.Employee.Employee parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static ProtoFiles.Employee.Employee parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static ProtoFiles.Employee.Employee parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static ProtoFiles.Employee.Employee parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static ProtoFiles.Employee.Employee parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static ProtoFiles.Employee.Employee parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static ProtoFiles.Employee.Employee parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static ProtoFiles.Employee.Employee parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static ProtoFiles.Employee.Employee parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(ProtoFiles.Employee.Employee prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code ProtoFiles.Employee.Employee}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:ProtoFiles.Employee.Employee)
      ProtoFiles.Employee.EmployeeOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return ProtoFiles.Employee.EmployeeOuterClass.internal_static_ProtoFiles_Employee_Employee_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return ProtoFiles.Employee.EmployeeOuterClass.internal_static_ProtoFiles_Employee_Employee_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              ProtoFiles.Employee.Employee.class, ProtoFiles.Employee.Employee.Builder.class);
    }

    // Construct using ProtoFiles.Employee.Employee.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      employeeId_ = 0;

      name_ = "";

      buildingCode_ = "";

      floorNumber_ = 0;

      salary_ = 0;

      department_ = "";

      cafeteriaCode_ = "";

      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return ProtoFiles.Employee.EmployeeOuterClass.internal_static_ProtoFiles_Employee_Employee_descriptor;
    }

    @java.lang.Override
    public ProtoFiles.Employee.Employee getDefaultInstanceForType() {
      return ProtoFiles.Employee.Employee.getDefaultInstance();
    }

    @java.lang.Override
    public ProtoFiles.Employee.Employee build() {
      ProtoFiles.Employee.Employee result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public ProtoFiles.Employee.Employee buildPartial() {
      ProtoFiles.Employee.Employee result = new ProtoFiles.Employee.Employee(this);
      result.employeeId_ = employeeId_;
      result.name_ = name_;
      result.buildingCode_ = buildingCode_;
      result.floorNumber_ = floorNumber_;
      result.salary_ = salary_;
      result.department_ = department_;
      result.cafeteriaCode_ = cafeteriaCode_;
      onBuilt();
      return result;
    }

    @java.lang.Override
    public Builder clone() {
      return super.clone();
    }
    @java.lang.Override
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.setField(field, value);
    }
    @java.lang.Override
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return super.clearField(field);
    }
    @java.lang.Override
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return super.clearOneof(oneof);
    }
    @java.lang.Override
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return super.setRepeatedField(field, index, value);
    }
    @java.lang.Override
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.addRepeatedField(field, value);
    }
    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof ProtoFiles.Employee.Employee) {
        return mergeFrom((ProtoFiles.Employee.Employee)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(ProtoFiles.Employee.Employee other) {
      if (other == ProtoFiles.Employee.Employee.getDefaultInstance()) return this;
      if (other.getEmployeeId() != 0) {
        setEmployeeId(other.getEmployeeId());
      }
      if (!other.getName().isEmpty()) {
        name_ = other.name_;
        onChanged();
      }
      if (!other.getBuildingCode().isEmpty()) {
        buildingCode_ = other.buildingCode_;
        onChanged();
      }
      if (other.floorNumber_ != 0) {
        setFloorNumberValue(other.getFloorNumberValue());
      }
      if (other.getSalary() != 0) {
        setSalary(other.getSalary());
      }
      if (!other.getDepartment().isEmpty()) {
        department_ = other.department_;
        onChanged();
      }
      if (!other.getCafeteriaCode().isEmpty()) {
        cafeteriaCode_ = other.cafeteriaCode_;
        onChanged();
      }
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      ProtoFiles.Employee.Employee parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (ProtoFiles.Employee.Employee) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private int employeeId_ ;
    /**
     * <code>uint32 Employee_Id = 1;</code>
     * @return The employeeId.
     */
    @java.lang.Override
    public int getEmployeeId() {
      return employeeId_;
    }
    /**
     * <code>uint32 Employee_Id = 1;</code>
     * @param value The employeeId to set.
     * @return This builder for chaining.
     */
    public Builder setEmployeeId(int value) {
      
      employeeId_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>uint32 Employee_Id = 1;</code>
     * @return This builder for chaining.
     */
    public Builder clearEmployeeId() {
      
      employeeId_ = 0;
      onChanged();
      return this;
    }

    private java.lang.Object name_ = "";
    /**
     * <code>string Name = 2;</code>
     * @return The name.
     */
    public java.lang.String getName() {
      java.lang.Object ref = name_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        name_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>string Name = 2;</code>
     * @return The bytes for name.
     */
    public com.google.protobuf.ByteString
        getNameBytes() {
      java.lang.Object ref = name_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        name_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>string Name = 2;</code>
     * @param value The name to set.
     * @return This builder for chaining.
     */
    public Builder setName(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      name_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>string Name = 2;</code>
     * @return This builder for chaining.
     */
    public Builder clearName() {
      
      name_ = getDefaultInstance().getName();
      onChanged();
      return this;
    }
    /**
     * <code>string Name = 2;</code>
     * @param value The bytes for name to set.
     * @return This builder for chaining.
     */
    public Builder setNameBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      name_ = value;
      onChanged();
      return this;
    }

    private java.lang.Object buildingCode_ = "";
    /**
     * <code>string Building_Code = 3;</code>
     * @return The buildingCode.
     */
    public java.lang.String getBuildingCode() {
      java.lang.Object ref = buildingCode_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        buildingCode_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>string Building_Code = 3;</code>
     * @return The bytes for buildingCode.
     */
    public com.google.protobuf.ByteString
        getBuildingCodeBytes() {
      java.lang.Object ref = buildingCode_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        buildingCode_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>string Building_Code = 3;</code>
     * @param value The buildingCode to set.
     * @return This builder for chaining.
     */
    public Builder setBuildingCode(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      buildingCode_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>string Building_Code = 3;</code>
     * @return This builder for chaining.
     */
    public Builder clearBuildingCode() {
      
      buildingCode_ = getDefaultInstance().getBuildingCode();
      onChanged();
      return this;
    }
    /**
     * <code>string Building_Code = 3;</code>
     * @param value The bytes for buildingCode to set.
     * @return This builder for chaining.
     */
    public Builder setBuildingCodeBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      buildingCode_ = value;
      onChanged();
      return this;
    }

    private int floorNumber_ = 0;
    /**
     * <code>.ProtoFiles.Employee.Floor Floor_Number = 4;</code>
     * @return The enum numeric value on the wire for floorNumber.
     */
    @java.lang.Override public int getFloorNumberValue() {
      return floorNumber_;
    }
    /**
     * <code>.ProtoFiles.Employee.Floor Floor_Number = 4;</code>
     * @param value The enum numeric value on the wire for floorNumber to set.
     * @return This builder for chaining.
     */
    public Builder setFloorNumberValue(int value) {
      
      floorNumber_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>.ProtoFiles.Employee.Floor Floor_Number = 4;</code>
     * @return The floorNumber.
     */
    @java.lang.Override
    public ProtoFiles.Employee.Floor getFloorNumber() {
      @SuppressWarnings("deprecation")
      ProtoFiles.Employee.Floor result = ProtoFiles.Employee.Floor.valueOf(floorNumber_);
      return result == null ? ProtoFiles.Employee.Floor.UNRECOGNIZED : result;
    }
    /**
     * <code>.ProtoFiles.Employee.Floor Floor_Number = 4;</code>
     * @param value The floorNumber to set.
     * @return This builder for chaining.
     */
    public Builder setFloorNumber(ProtoFiles.Employee.Floor value) {
      if (value == null) {
        throw new NullPointerException();
      }
      
      floorNumber_ = value.getNumber();
      onChanged();
      return this;
    }
    /**
     * <code>.ProtoFiles.Employee.Floor Floor_Number = 4;</code>
     * @return This builder for chaining.
     */
    public Builder clearFloorNumber() {
      
      floorNumber_ = 0;
      onChanged();
      return this;
    }

    private int salary_ ;
    /**
     * <code>uint32 Salary = 5;</code>
     * @return The salary.
     */
    @java.lang.Override
    public int getSalary() {
      return salary_;
    }
    /**
     * <code>uint32 Salary = 5;</code>
     * @param value The salary to set.
     * @return This builder for chaining.
     */
    public Builder setSalary(int value) {
      
      salary_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>uint32 Salary = 5;</code>
     * @return This builder for chaining.
     */
    public Builder clearSalary() {
      
      salary_ = 0;
      onChanged();
      return this;
    }

    private java.lang.Object department_ = "";
    /**
     * <code>string Department = 6;</code>
     * @return The department.
     */
    public java.lang.String getDepartment() {
      java.lang.Object ref = department_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        department_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>string Department = 6;</code>
     * @return The bytes for department.
     */
    public com.google.protobuf.ByteString
        getDepartmentBytes() {
      java.lang.Object ref = department_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        department_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>string Department = 6;</code>
     * @param value The department to set.
     * @return This builder for chaining.
     */
    public Builder setDepartment(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      department_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>string Department = 6;</code>
     * @return This builder for chaining.
     */
    public Builder clearDepartment() {
      
      department_ = getDefaultInstance().getDepartment();
      onChanged();
      return this;
    }
    /**
     * <code>string Department = 6;</code>
     * @param value The bytes for department to set.
     * @return This builder for chaining.
     */
    public Builder setDepartmentBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      department_ = value;
      onChanged();
      return this;
    }

    private java.lang.Object cafeteriaCode_ = "";
    /**
     * <code>string cafeteria_code = 7;</code>
     * @return The cafeteriaCode.
     */
    public java.lang.String getCafeteriaCode() {
      java.lang.Object ref = cafeteriaCode_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        cafeteriaCode_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>string cafeteria_code = 7;</code>
     * @return The bytes for cafeteriaCode.
     */
    public com.google.protobuf.ByteString
        getCafeteriaCodeBytes() {
      java.lang.Object ref = cafeteriaCode_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        cafeteriaCode_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>string cafeteria_code = 7;</code>
     * @param value The cafeteriaCode to set.
     * @return This builder for chaining.
     */
    public Builder setCafeteriaCode(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      cafeteriaCode_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>string cafeteria_code = 7;</code>
     * @return This builder for chaining.
     */
    public Builder clearCafeteriaCode() {
      
      cafeteriaCode_ = getDefaultInstance().getCafeteriaCode();
      onChanged();
      return this;
    }
    /**
     * <code>string cafeteria_code = 7;</code>
     * @param value The bytes for cafeteriaCode to set.
     * @return This builder for chaining.
     */
    public Builder setCafeteriaCodeBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      cafeteriaCode_ = value;
      onChanged();
      return this;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:ProtoFiles.Employee.Employee)
  }

  // @@protoc_insertion_point(class_scope:ProtoFiles.Employee.Employee)
  private static final ProtoFiles.Employee.Employee DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new ProtoFiles.Employee.Employee();
  }

  public static ProtoFiles.Employee.Employee getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<Employee>
      PARSER = new com.google.protobuf.AbstractParser<Employee>() {
    @java.lang.Override
    public Employee parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return new Employee(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<Employee> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<Employee> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public ProtoFiles.Employee.Employee getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

