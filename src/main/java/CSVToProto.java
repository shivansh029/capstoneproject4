import ProtoFiles.Attendance.Attendance;
import ProtoFiles.Attendance.AttendanceList;
import ProtoFiles.Building.BuildingList;
import ProtoFiles.Employee.Employee;
import ProtoFiles.Employee.EmployeeList;
import com.assignment4a.util.Constant;

import java.io.*;

public class CSVToProto {

    public void readCsvData(String FilePath, String FileType) {
        try {

            FileReader fr = new FileReader(FilePath);
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            boolean header = true;

            while ((line = br.readLine()) != null) {
                if (header) {
                    // avoiding first line of csv file
                    header = false;
                    continue;
                }
                String[] data = line.replaceAll("\"", "")
                        .replaceAll("\\\"", "")
                        .replaceAll(" ,", "")
                        .replaceAll(", ", ",")
                        .split(",");

                if (FileType.equals("Employee"))
                    printEmployeeData(data);
                else if(FileType.equals("Building"))
                    printBuildingData(data);
                else{
                    printAttendanceData(data);
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printAttendanceData(String[] attendanceData) {

        if(attendanceData.length == 0){
            return;
        }

        Attendance.Builder attendance = Attendance.newBuilder();
        attendance.setEmployeeID(Integer.parseInt(attendanceData[0]))
                .setDate(attendanceData[1]);

        System.out.println(attendance.toString());

        AttendanceList.Builder attendanceList = AttendanceList.newBuilder();

        try{
            attendanceList.mergeFrom(new FileInputStream(Constant.AttendanceSerializedFilePath));
        } catch(FileNotFoundException e){
            System.out.println(Constant.AttendanceSerializedFilePath + " File not found. Creating a new File");
        } catch(IOException e){
            e.printStackTrace();
        }

        attendanceList.addAttendance(attendance.build());

        try(FileOutputStream output = new FileOutputStream(Constant.AttendanceSerializedFilePath)){
            attendanceList.build().writeTo(output);
            output.close();
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    private void printBuildingData(String[] buildingData) {
        if (buildingData.length == 0)
            return;
        ProtoFiles.Building.Building.Builder building = ProtoFiles.Building.Building.newBuilder();
        building.setBuildingCode(buildingData[0])
                .setTotalFloors(Integer.parseInt(buildingData[1]))
                .setTotalCompanies(Integer.parseInt(buildingData[2]))
                .setCafeteriaCode(buildingData[3]);
        System.out.println(building.toString());
        BuildingList.Builder buildingList = BuildingList.newBuilder();
        try {
            buildingList.mergeFrom(new FileInputStream(Constant.BuildingSerializedFilePath));
        } catch (FileNotFoundException e) {
            System.out.println(Constant.BuildingSerializedFilePath + ": File not found.  Creating a new file.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        buildingList.addBuilding(building.build());
        try (FileOutputStream output = new FileOutputStream(Constant.BuildingSerializedFilePath)) {
            buildingList.build().writeTo(output);
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void printEmployeeData(String[] EmployeeData) {
        if (EmployeeData.length == 0) {
            return;
        }
        Employee.Builder employee = Employee.newBuilder();
        employee.setEmployeeId(Integer.parseInt(EmployeeData[0]))
                .setName(EmployeeData[1])
                .setBuildingCode(EmployeeData[2])
                .setFloorNumberValue(Integer.parseInt(EmployeeData[3]))
                .setSalary(Integer.parseInt(EmployeeData[4]))
                .setDepartment(EmployeeData[5]);
        System.out.println(employee.toString());

        EmployeeList.Builder employeeList = EmployeeList.newBuilder();
        try {
            employeeList.mergeFrom(new FileInputStream(Constant.EmployeeSerializedFilePath));
        } catch (FileNotFoundException e) {
            System.out.println(Constant.EmployeeSerializedFilePath + ": File not found.  Creating a new file.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        employeeList.addEmployees(employee.build());
        try (FileOutputStream output = new FileOutputStream(Constant.EmployeeSerializedFilePath)) {
            employeeList.build().writeTo(output);
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String args[]) {
        String EmployeeCSVFilePath = Constant.EmployeeCsvFilePath;
        String BuildingCSVFilePath = Constant.BuildingCsvFilePath;

        CSVToProto obj = new CSVToProto();

        obj.readCsvData(EmployeeCSVFilePath, "Employee");
        obj.readCsvData(BuildingCSVFilePath, "Building");

        String AttendanceDataCSVFilePath = Constant.AttendanceCSVFilePath;
        obj.readCsvData(AttendanceDataCSVFilePath, "Attendance");

    }
}
