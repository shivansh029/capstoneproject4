package com.assignment4b;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.mapred.TableMap;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.assignment4b.util.Constant.BUILDING_TABLE;
import static com.assignment4b.util.Constant.EMPLOYEE_TABLE;

public class CafeteriaMRDriver {

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {

        List<Scan> scans = new ArrayList<>();
        Scan scan = new Scan();
        scan.setCaching(500);
        scan.setCacheBlocks(false);
        scan.setAttribute("scan.attributes.table.name", Bytes.toBytes(EMPLOYEE_TABLE));
        scans.add(scan);

        scan = new Scan();
        scan.setCaching(500);
        scan.setCacheBlocks(false);
        scan.setAttribute("scan.attributes.table.name", Bytes.toBytes(BUILDING_TABLE));
        scans.add(scan);

        Configuration config = new Configuration();
        Job job = Job.getInstance(config);
        job.setJobName("Join on Building Code");
        job.setJarByClass(CafeteriaMRDriver.class);

        TableMapReduceUtil.initTableMapperJob(scans, CafeteriaMapper.class, Text.class, Result.class, job);

        TableMapReduceUtil.initTableReducerJob(EMPLOYEE_TABLE, CafeteriaReducer.class, job);

        boolean b = job.waitForCompletion(true);

        System.out.println(b);

        if(job.isSuccessful()){
            System.out.println("Success. Cafeteria code added to : " + EMPLOYEE_TABLE);
        }


    }
}
