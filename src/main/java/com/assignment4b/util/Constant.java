package com.assignment4b.util;

import org.apache.hadoop.hbase.util.Bytes;

public class Constant {

    public static byte[] EMPLOYEE_TABLE_BYTES = Bytes.toBytes("employeeData");
    public static byte[] BUILDING_TABLE_BYTES = Bytes.toBytes("buildingData");
    public static final String EMPLOYEE_COLUMN_FAMILY = "employee";
    public static final String BUILDING_COLUMN_FAMILY = "building";
    public static final String EMPLOYEE_TABLE = "employeeData";
    public static final String BUILDING_TABLE = "buildingData";
    public static final String ASSIGNMENT_4_2_HDFS_OUTPUT_PATH = "hdfs://localhost:9000/assignment4b";

}
