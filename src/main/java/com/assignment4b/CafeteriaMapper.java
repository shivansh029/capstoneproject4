package com.assignment4b;

import ProtoFiles.Building.Building;
import ProtoFiles.Employee.Employee;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableSplit;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.Arrays;

import static com.assignment4b.util.Constant.BUILDING_TABLE_BYTES;
import static com.assignment4b.util.Constant.EMPLOYEE_TABLE_BYTES;

public class CafeteriaMapper extends TableMapper<Text, Result> {

    public CafeteriaMapper(){
        System.out.println("Mapper called");
    }

    @Override
    protected void map(ImmutableBytesWritable key, Result value,
                       Mapper<ImmutableBytesWritable, Result, Text, Result>.Context context) throws IOException, InterruptedException {

        TableSplit tableSplit = (TableSplit) context.getInputSplit();
        byte[] tableName = tableSplit.getTableName();

        if(Arrays.equals(tableName, EMPLOYEE_TABLE_BYTES)){
            byte[] columnFamily = Bytes.toBytes("employee");
            byte[] cqBuildingCode = Bytes.toBytes("building_code");
            String building_code = Bytes.toString(value.getValue(columnFamily, cqBuildingCode));
            context.write(new Text(building_code), value);
        }

        else if(Arrays.equals(tableName, BUILDING_TABLE_BYTES)){
            byte[] columnFamily = Bytes.toBytes("building");
            byte[] cqBuildingCode = Bytes.toBytes("building_code");
            String building_code = Bytes.toString(value.getValue(columnFamily, cqBuildingCode));
            context.write(new Text(building_code), value);
        }
    }
}
