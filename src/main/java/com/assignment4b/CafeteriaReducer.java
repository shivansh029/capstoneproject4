package com.assignment4b;

import ProtoFiles.Employee.Employee;
import org.apache.hadoop.hbase.client.Mutation;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.assignment4b.util.Constant.BUILDING_COLUMN_FAMILY;
import static com.assignment4b.util.Constant.EMPLOYEE_COLUMN_FAMILY;


public class CafeteriaReducer extends TableReducer<Text, Result, ImmutableBytesWritable> {

    public CafeteriaReducer() {
        System.out.println("Reducer called.");
    }

    Map<String, String> buildingCafe = new HashMap<>();

    @Override
    protected void reduce(Text key,
                          Iterable<Result> values, Reducer<Text, Result, ImmutableBytesWritable, Mutation>.Context context) throws IOException, InterruptedException {

        List<Employee> employeeList = new ArrayList<>();

        for(Result result : values){
            if(result.containsColumn(Bytes.toBytes(EMPLOYEE_COLUMN_FAMILY), Bytes.toBytes("name"))){

                byte[] columnFamily = Bytes.toBytes("employee");
                byte[] cqEmpID = Bytes.toBytes("employee_id");
                byte[] cqBuildingCode = Bytes.toBytes("building_code");

                Employee.Builder employee = Employee.newBuilder();
                employee.setEmployeeId(Integer.parseInt(Bytes.toString(result.getValue(columnFamily, cqEmpID))))
                        .setBuildingCode(Bytes.toString(result.getValue(columnFamily, cqBuildingCode)));

                employeeList.add(employee.build());
            }

            else if(result.containsColumn(Bytes.toBytes(BUILDING_COLUMN_FAMILY), Bytes.toBytes("building_code"))){

                byte[] columnFamily = Bytes.toBytes("building");
                byte[] cqBuildingCode = Bytes.toBytes("building_code");
                byte[] cqCafeteriaCode = Bytes.toBytes("cafeteria_code");

                String building_id = Bytes.toString(result.getValue(columnFamily, cqBuildingCode));
                String cafeteria_code = Bytes.toString(result.getValue(columnFamily, cqCafeteriaCode));

                if(!buildingCafe.containsKey(building_id)){
                    buildingCafe.put(building_id, cafeteria_code);
                }
            }
        }

        for(Employee employee : employeeList){
            Put put = insertCafeteriaCode(employee);
            int employee_id = employee.getEmployeeId();
            context.write(null, put);
        }

    }

    public Put insertCafeteriaCode(Employee employee){

        String building_code = employee.getBuildingCode();
        String employee_id = String.valueOf(employee.getEmployeeId());
        System.out.println("Employee : " + employee_id + " - " + building_code + " - " + buildingCafe.get(building_code));
        Put put = new Put(Bytes.toBytes(employee_id));
        put.addColumn(Bytes.toBytes("employee"), Bytes.toBytes("cafeteria_code"), Bytes.toBytes(buildingCafe.get(building_code)));

        return put;
    }
}
