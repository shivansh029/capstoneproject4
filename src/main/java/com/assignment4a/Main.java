package com.assignment4a;

import java.io.IOException;

import static com.assignment4a.util.Constant.*;

public class Main {
    public static void main(String args[]) throws IOException {
        UploadFilesToHDFS obj=new UploadFilesToHDFS();
        obj.storeSerializedFile(EmployeeSerializedFilePath, EMPLOYEE_HDFS_OUTPUT_PATH);
        obj.storeSerializedFile(BuildingSerializedFilePath, BUILDING_HDFS_OUTPUT_PATH);
        obj.storeSerializedFile(AttendanceSerializedFilePath, ATTENDANCE_HDFS_OUTPUT_PATH);

        HBaseTableCreator hbaseTableCreator=new HBaseTableCreator();
        hbaseTableCreator.createTableBuilding(BUILDING_TABLE_NAME);
        hbaseTableCreator.createTableEmployee(EMPLOYEE_TABLE_NAME);
        hbaseTableCreator.createTableAttendance(ATTENDANCE_TABLE_NAME);

    }
}
