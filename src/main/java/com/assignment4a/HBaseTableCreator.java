package com.assignment4a;
import static com.assignment4a.util.Constant.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import java.io.*;


public class HBaseTableCreator {
    public void createTableBuilding(String tableNameToCreate) throws IOException {

        Configuration config = HBaseConfiguration.create();
        Connection connection = ConnectionFactory.createConnection(config);
        Admin hAdmin = connection.getAdmin();
        if (hAdmin.tableExists(TableName.valueOf(tableNameToCreate))) {
            System.out.println(tableNameToCreate + "table already exists.");
            return;
        }
        TableName tname = TableName.valueOf(tableNameToCreate);
        TableDescriptorBuilder tableDescBuilder = TableDescriptorBuilder.newBuilder(tname);

        ColumnFamilyDescriptorBuilder columnDescBuilderBuilding = ColumnFamilyDescriptorBuilder
                .newBuilder(Bytes.toBytes(COLUMN_FAMILY_BUILDING));
        tableDescBuilder.setColumnFamily(columnDescBuilderBuilding.build());

        tableDescBuilder.build();

        hAdmin.createTable(tableDescBuilder.build());
        System.out.println(tableNameToCreate + TABLE_CREATED);
    }

    public void createTableEmployee(String tableNameToCreate) throws IOException {

        Configuration config = HBaseConfiguration.create();
        Connection connection = ConnectionFactory.createConnection(config);
        Admin hAdmin = connection.getAdmin();
        if (hAdmin.tableExists(TableName.valueOf(tableNameToCreate))) {
            System.out.println(tableNameToCreate + " table already exists.");
            return;
        }

        TableName tname = TableName.valueOf(tableNameToCreate);
        TableDescriptorBuilder tableDescBuilder = TableDescriptorBuilder.newBuilder(tname);

        ColumnFamilyDescriptorBuilder columnDescBuilderEmployee = ColumnFamilyDescriptorBuilder
                .newBuilder(Bytes.toBytes(COLUMN_FAMILY_EMPLOYEE));
        tableDescBuilder.setColumnFamily(columnDescBuilderEmployee.build());
        tableDescBuilder.build();

        hAdmin.createTable(tableDescBuilder.build());
        System.out.println(tableNameToCreate + TABLE_CREATED);
    }

    public void createTableAttendance(String tableNameToCreate) throws IOException {

        Configuration configuration = HBaseConfiguration.create();
        Connection connection = ConnectionFactory.createConnection(configuration);
        Admin hAdmin = connection.getAdmin();

        if(hAdmin.tableExists(TableName.valueOf(tableNameToCreate))){
            System.out.println(tableNameToCreate + " table already exists.");
            return;
        }

        TableName tName = TableName.valueOf(tableNameToCreate);
        TableDescriptorBuilder tableDescriptorBuilder = TableDescriptorBuilder.newBuilder(tName);

        ColumnFamilyDescriptorBuilder columnFamilyDescriptorBuilder = ColumnFamilyDescriptorBuilder.newBuilder(Bytes.toBytes(COLUMN_FAMILY_ATTENDANCE));
        tableDescriptorBuilder.setColumnFamily(columnFamilyDescriptorBuilder.build());
        tableDescriptorBuilder.build();

        hAdmin.createTable(tableDescriptorBuilder.build());
        System.out.println(tableNameToCreate + " table created.");
    }
}

