package com.assignment4a.util;

public class Constant {

    public static final String rootUrl = "hdfs://localhost:9000";
    public static final String HOME = System.getProperty("user.home");
    public static final String EMPLOYEE_HDFS_OUTPUT_PATH=rootUrl+"/Assignment4/employeeSerializedFile";
    public static final String BUILDING_HDFS_OUTPUT_PATH=rootUrl+"/Assignment4/buildingSerializedFile";
    public static final String EMPLOYEE_TABLE_NAME="employeeData";
    public static final String BUILDING_TABLE_NAME="buildingData";
    public static final String BUILDING = "building";
    public static final String BUILDING_DETAILS = "building_details";
    public static final String EMPLOYEE = "employee";
    public static final String EMPLOYEE_DETAIL = "employee_details";
    public static final String EmployeeCsvFilePath = HOME + "/CapstoneProjects/capstoneproject4/src/main/resources/EmployeeData.csv";
    public static final String BuildingCsvFilePath = HOME + "/CapstoneProjects/capstoneproject4/src/main/resources/BuildingData.csv";
    public static final String EmployeeSerializedFilePath = HOME + "/CapstoneProjects/capstoneproject4/src/main/resources/ProtoOutputFiles/employeeSerializedFile";
    public static final String BuildingSerializedFilePath = HOME + "/CapstoneProjects/capstoneproject4/src/main/resources/ProtoOutputFiles/buildingSerializedFile";
    public static final String COLUMN_FAMILY_EMPLOYEE = "employee";
    public static final String COLUMN_FAMILY_BUILDING = "building";
    public static final String TABLE_ALREADY_EXISTS = "Table already exists";
    public static final String TABLE_CREATED =  " table created";
    public static final String EMPLOYEE_HFILE_OUTPUT_PATH = "hdfs://localhost:9000/Assignment4/HFile/employee_hfile/";
    public static final String BUILDING_HFILE_OUTPUT_PATH = "hdfs://localhost:9000/Assignment4/HFile/building_hfile/";
    public static final String DEFAULT_FS = "fs.defaultFS";
    public static final String HDFS_INPUT_URL = "hdfs://localhost:9000/";
    public static final String HDFS_IMPL = "fs.hdfs.impl";
    public static final String FILE_IMPL = "fs.file.impl";
    public static final String BULK_LOADING_MESSAGE = "Bulk Loading HBase Table::";


    public static final String AttendanceSerializedFilePath = HOME + "/CapstoneProjects/capstoneproject4/src/main/resources/ProtoOutputFiles/attendanceSerializedFile";
    public static final String AttendanceCSVFilePath = HOME + "/CapstoneProjects/capstoneproject4/src/main/resources/AttendanceData.csv";
    public static final String ATTENDANCE_HDFS_OUTPUT_PATH = rootUrl+"/Assignment4/attendanceSerializedFile";
    public static final String COLUMN_FAMILY_ATTENDANCE = "attendance";
    public static final String ATTENDANCE_TABLE_NAME = "attendanceData";
    public static final String ATTENDANCE_HFILE_OUTPUT_PATH = "hdfs://localhost:9000/Assignment4/HFile/attendance_hfile/";


}
