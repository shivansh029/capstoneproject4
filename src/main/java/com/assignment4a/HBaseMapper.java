package com.assignment4a;

import static com.assignment4a.util.Constant.*;

import ProtoFiles.Attendance.Attendance;
import ProtoFiles.Attendance.AttendanceList;
import ProtoFiles.Building.Building;
import ProtoFiles.Building.BuildingList;
import ProtoFiles.Employee.Employee;
import ProtoFiles.Employee.EmployeeList;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Mapper;

import java.util.*;
import java.util.stream.Collectors;

class EmployeeMapper extends Mapper<NullWritable, BytesWritable, ImmutableBytesWritable, Put> {

    ImmutableBytesWritable TABLE_NAME_TO_INSERT = new ImmutableBytesWritable(Bytes.toBytes(EMPLOYEE_TABLE_NAME));

    public void map(NullWritable key, BytesWritable value, Context context) {
        try {

            byte b[] = value.getBytes();
            EmployeeList employeeList = EmployeeList.parseFrom(Arrays.copyOf(value.getBytes(), value.getLength()));
            for (Employee employee : employeeList.getEmployeesList()) {
                int employee_id = employee.getEmployeeId();

                Put put = new Put(Bytes.toBytes(String.valueOf(employee_id)));
                put.addColumn(Bytes.toBytes(COLUMN_FAMILY_EMPLOYEE), Bytes.toBytes("employee_id"), Bytes.toBytes(String.valueOf(employee_id)))
                        .addColumn(Bytes.toBytes(COLUMN_FAMILY_EMPLOYEE), Bytes.toBytes("name"), Bytes.toBytes(employee.getName()))
                        .addColumn(Bytes.toBytes(COLUMN_FAMILY_EMPLOYEE), Bytes.toBytes("building_code"), Bytes.toBytes(employee.getBuildingCode()))
                        .addColumn(Bytes.toBytes(COLUMN_FAMILY_EMPLOYEE), Bytes.toBytes("floor_number"), Bytes.toBytes(String.valueOf(employee.getFloorNumberValue())))
                        .addColumn(Bytes.toBytes(COLUMN_FAMILY_EMPLOYEE), Bytes.toBytes("salary"), Bytes.toBytes(String.valueOf(employee.getSalary())))
                        .addColumn(Bytes.toBytes(COLUMN_FAMILY_EMPLOYEE), Bytes.toBytes("department"), Bytes.toBytes(employee.getDepartment()));

                context.write(TABLE_NAME_TO_INSERT, put);
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}

class BuildingMapper extends Mapper<NullWritable, BytesWritable, ImmutableBytesWritable, Put> {

    ImmutableBytesWritable TABLE_NAME_TO_INSERT = new ImmutableBytesWritable(Bytes.toBytes(BUILDING_TABLE_NAME));

    public void map(NullWritable key, BytesWritable value, Context context) {
        try {

            byte b[] = value.getBytes();
            BuildingList buildingList = BuildingList.parseFrom(Arrays.copyOf(value.getBytes(), value.getLength()));

            for (Building building : buildingList.getBuildingList()) {

                Put put = new Put(Bytes.toBytes(building.getBuildingCode()));
                put.addColumn(Bytes.toBytes(COLUMN_FAMILY_BUILDING), Bytes.toBytes("building_code"), Bytes.toBytes(building.getBuildingCode()))
                        .addColumn(Bytes.toBytes(COLUMN_FAMILY_BUILDING), Bytes.toBytes("floors"), Bytes.toBytes(String.valueOf(building.getTotalFloors())))
                        .addColumn(Bytes.toBytes(COLUMN_FAMILY_BUILDING), Bytes.toBytes("companies"), Bytes.toBytes(String.valueOf(building.getTotalCompanies())))
                        .addColumn(Bytes.toBytes(COLUMN_FAMILY_BUILDING), Bytes.toBytes("cafeteria_code"), Bytes.toBytes(building.getCafeteriaCode()));

                context.write(TABLE_NAME_TO_INSERT, put);


            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}

class AttendanceMapper extends Mapper<NullWritable, BytesWritable, ImmutableBytesWritable, Put> {

    ImmutableBytesWritable TABLE_NAME_TO_INSERT = new ImmutableBytesWritable(Bytes.toBytes(ATTENDANCE_TABLE_NAME));

    public void map(NullWritable key, BytesWritable value, Context context){

        try{

            byte[] b = value.getBytes();

            AttendanceList attendanceList = AttendanceList.parseFrom(Arrays.copyOf(value.getBytes(), value.getLength()));

            int i = 0;
            for(int j = 1; j <= 100; j++){
                int employeeID = j;
                List<Attendance> newList = attendanceList.getAttendanceList()
                                                         .stream()
                                                         .filter(m -> m.getEmployeeID() == employeeID)
                                                         .collect(Collectors.toList());

                AttendanceList dbo = AttendanceList.newBuilder().addAllAttendance(newList).build();

                if(newList.size() > 0){
                    Put put = new Put(Bytes.toBytes(String.valueOf(i + "")));
                    put.addColumn(Bytes.toBytes(COLUMN_FAMILY_ATTENDANCE), Bytes.toBytes("proto_obj"), dbo.toByteArray());

                    context.write(TABLE_NAME_TO_INSERT, put);

                    i++;
                }
            }

        }catch (Exception exception){
            exception.printStackTrace();
        }
    }
}